# oauth2-proxy-dev

OAuth2 Proxy for local development on remote server with Keycloak and NGINX.

Reference: https://github.com/oauth2-proxy/oauth2-proxy/tree/master/contrib/local-environment

## Set Domain Values

You need to replace `OAPKNG_DOMAIN` with whatever your domain is in the following files:

* keycloak/master-realm.json
* nginx.conf
* oauth.cfg

You can use the following to do so on linux.

```bash
# set domain value
export DOMAIN=whatever.blah

# replace domain in static files
sed -i -r "s|OAPKNG_DOMAIN|${DOMAIN}|g" keycloak/master-realm.json
sed -i -r "s|OAPKNG_DOMAIN|${DOMAIN}|g" nginx.conf 
sed -i -r "s|OAPKNG_DOMAIN|${DOMAIN}|g" oauth.cfg 
```

## Run Locally

```bash
## create docker network
docker network create oauth2-proxy

# run local keycloak
# taken from docker-compose-keycloak.yml
# https://github.com/oauth2-proxy/oauth2-proxy/tree/master/contrib/local-environment
# sleighzy/keycloak if running on m1/m2 mac
docker run --rm \
  --name keycloak \
  -e KEYCLOAK_USER=admin@example.com \
  -e KEYCLOAK_PASSWORD=password \
  -v $(pwd)/keycloak:/realm-config \
  -p 9080:9080/tcp \
  jboss/keycloak:10.0.0 -b 0.0.0.0 -Djboss.socket.binding.port-offset=1000 -Dkeycloak.migration.action=import -Dkeycloak.migration.provider=dir -Dkeycloak.migration.dir=\/realm-config -Dkeycloak.migration.strategy=IGNORE_EXISTING

# run local redis for session storage
docker run --rm \
  --network oauth2-proxy \
  --name redis \
  --hostname redis \
  redis

# run local oauth2 proxy
# taken from docker-compose-keycloak.yml
# https://github.com/oauth2-proxy/oauth2-proxy/tree/master/contrib/local-environment
docker run --rm \
  --network oauth2-proxy \
  --name oauth2-proxy \
  --hostname oauth2-proxy \
  -v $(pwd)/oauth.cfg:/oauth2-proxy.cfg \
  quay.io/oauth2-proxy/oauth2-proxy:v7.4.0 --config /oauth2-proxy.cfg

# run local nginx
# taken from docker-compose-nginx.yml
# https://github.com/oauth2-proxy/oauth2-proxy/tree/master/contrib/local-environment
docker run --rm \
  --network oauth2-proxy \
  --name nginx \
  -v $(pwd)/nginx-http.conf:/etc/nginx/nginx.conf \
  -v $(pwd)/nginx.conf:/etc/nginx/conf.d/default.conf \
  -p 80:80/tcp \
  nginx:1.18
```
